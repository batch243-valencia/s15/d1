// console.log("Hello World");

let fName = "John";
let lName = "Smith";
let age = 30;
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'] 
let workAddress= {
    houseNumber: "32",
    street: 'Washington',
    city: 'Lincoln',
    state: 'Nebraska'
};
console.log("First Name: " + fName);
console.log("Last Name: " + lName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);
{

    let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);
}

let profile = {
		username: "captain_america",
		fullName: "Steve Roger",
		age: 40,
		isActive: false,
	}
    console.log("My Full Profile: ")
	console.log(profile);


	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
