// console.log("Hellow World")

// [Section] syntax, statments and comments
// Statements in prgoramming are instructions that we tell the computer to perform
// JS statement usually end with semicolon(;)
// semicolons are not required in js, but we will use it to help us train to locate where a statement ends
//A Syntax in programming, it is the set of rules of rules that we describes how statements must be constructed.
// all line/blocks of codes must be written in a specific manner of work.
// This is due to how these codes where initially programmed to function in certain manner.
// Comments are parts of the code that gets ignored by the language.

// comments are meant to describe the written code.


/*
There are two type of comments:
1. The Single-line Comment denoted by two slashes
2. the multi-line comment denoted by slashes and asterisk
 */

// [Section Variables

// Variables are use to contain data.
// any information that is used by an application is stored in what we call the "memory"
// when we create variables, certain portions of a device's memory is given a name that we call variables.
// this makes it easier for us to associate information stored in our devices to actual "names" about information.

// Declaring variables
// Declaring variables - it tells our devices that a variable name is created and is ready to store data.
// Declaring a variable without giving it a value will automtically assign it with the value of "undefined", meaning the variable's value was not defined
    // syntax
        // let /const variableName;
    let myVariable;
// Console.Log() is useful for printing values of variables or certain results of code into the google chrome browsers console.
// constant use of this throughtout developing an application will save us time and builds good habit in always checking for the output of our code.
console.log(myVariable);



/*
Guide in writing variables:
    1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
    2. Variable names should start with lowercase character, use camelCase for multiple words.
    3. for constant variables, use the 'const' keywords.
    using let: we can change the value of the variable.
    using const: we cannot change the value of the variables.
    4. Variable names should be indicative (descriptive) of the value being store to avoid confusion.
*/

// Declare and initialize variables
// Initializing variables - the instance when a variable is given it's initial/ starting value
    // Syntax
        // let/const variableName = value;
let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);
productPrice = 222;
console.log(productPrice);

const interest = 3.539;
console.log(interest);
// interest = 5;
// console.log(interest);

// reassigning variable values;
// reasigning variable means chaing it's initial or previous value in another value.
    // syntax
        // VariableName = newValue;
productName="Laptop";
console.log(productName);

// let variable cannot be re-declared within its scope.

// let productName= "test";

// Values of constants cannot be changed and will simple return an error.

// Reassining variables and Initializing variables

let supplier;
// initialization
supplier = "Zuitt Store";
// Reassignment
supplier = "Zutt merch";

// var vs let/const
    // some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var.
    // var - is also used in declaring variable. but var is an Ecmascript1 feature [ES1 (Javascript 1997]
    // let/const was intruduced as a new feature in ES6(2016)
    // What makes let/const dfferent var?

a=5;
console.log(a);


// error 
// b=5;
// console.log(b);
// let b; 

    // let/const local/global scope

    let outerVariable="hello from the other side";
    {
        let innervariable = "hellow from the block";

        console.log(outerVariable);
        console.log(innervariable);
    }
    console.log(outerVariable);
    // console.log(innervariable);


    // multiple variable declarations
    // multiple variable can be declared in one line.

    let productCode ="Dc017", productBrand = "Dell";

    console.log(productCode,productBrand);
    console.log(productCode);
    console.log(productBrand);

    // using a variable with a reserved keyword. (e.g.)
    // const let = "hello"; 
    
    // [Sections] Data types
    // Strings - are series of characters that create a word, phrase, a sentence or anything related to creating text.
    // Strings in Javascript can be writted using either singe (') and double (") quote.
    // In other programming Languages, only the double can be used for creating strings.

    let country = "Philippines";
    let province = 'Metro Manila';

    // Concatenate
    // Multiple string values can be combined to create a single string using the "+" symbols.

    let fullAddress = province + ", " + country;
    console.log(fullAddress);

    let greeting = "I Live in the" + country;

    // the escape characters (\)
    //  "\n" refers to creating a new line between next
    let mailAddress ="Metro Manila\nPhilippines";
    console.log(mailAddress)
let message =  "John's employees went home early."
console.log(message)
//error 
// message = '"John's employees went home early."';
console.log(message);

// Numbers
// Integers/whole numbers
let count = "25";
let headCount = 26;
console.log(count);
console.log(headCount);
// Decimal Numbers / Fractions
let grade = 98.7;
console.log(grade);
// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);
// concat STRING + Int
console.log(grade+headCount);
// Boolean
// Boolean values are normally used to store values relating to the state of certain things.
let isMarried = false;
let isGoodConduct=true;
console.log(isMarried);
console.log(isGoodConduct);

// Because true is 1 when auto-converted to a number. Javascript's dynamic type system in action.
console.log(isGoodConduct+isMarried);

// Arrays
// Arrays can store different data types but is normally use to store similar data types.

    // Similar data types
        // Syntax
        // let/const arrayName = [elementA,elementB, elementC, ...];
let grades= [98.7,92.1,90.2, 94.22];
console.log(grades);
// log 2nd grade 
// zero based
console.log(grades[1]);
// different data types
// storing different data types inside an array is not recommended because it willnot make sense in the context of programming

// Objects
// objects are another kind of data type that used to mimic real world objects/items
// thet are used to create a complex data contains pieces of information that are relevant to each other.
    // syntax:
        /* let/const onjectName = {
        propertyA: value,
        propertyB: value
        };
        */
let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact:["0917234567","8546 465156"],
    address:{
        houseNumber: '345',
        city:"manila"
    }
}
console.log(person);
// accessing obj.key
// accessing obj.key[i]
console.log(person.contact[0]);
// typeof operator is used to determine
console.log(typeof person);
console.log(typeof person.fullName);


/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */
const anime = ['one piece' , 'one punch man', 'attack on titans'];
// error assignment to constant variable
// anime = ['kimetsu no yaiba'];
console.log(anime);
anime[0] = "kimetsu no yaiba";
// 
anime[5] = "dxd";
console.log(anime);
// null simple means that a data type was assigned to a variable but it does not hold any value/amount or nullified
let spouse = null;
console.log(spouse);
// undefined
// represent the state of a variable that has been declared but w/o value
// undefined vs  Null
// the difference between undefined and nulls is that for undefined, a variable was created but not provided a value
    // null means that the variable was created and was assigned a value that does not hold and value/amount